package com.mavzapps.sunshine;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * A placeholder fragment containing a simple view.
 */
public class DetailActivityFragment extends Fragment {

    public DetailActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_detail, container, false);
        DetailActivity detailActivity = (DetailActivity)getActivity();
        //String stuff = detailActivity.getForecast();
        String stuff = getActivity().getIntent().getStringExtra("forecast");
        TextView textView = (TextView)rootView.findViewById(R.id.detailTextView);
        textView.setText(stuff);
        return rootView;
    }
}
